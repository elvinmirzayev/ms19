package az.ingrees.demo.controller;


import az.ingrees.demo.service.StudentService;
import az.ingrees.demo.service.StudentServiceImp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/employee")

public class StudentController {
    @Autowired
    private StudentService studentService;
    @GetMapping("/{id}")
    public Student get(@PathVariable Long id){
        return studentService.get(id);
    }
    @PostMapping
    public Student create(@RequestBody Student employee){
        return studentService.create(employee);
    }

    @PutMapping
    public Student update(@RequestBody Student employee){
        return studentService.update(employee);
    }
    @DeleteMapping("/employee{id}delete")
    public void delete(@PathVariable Long id){
        studentService.delete(id);
    }
}


