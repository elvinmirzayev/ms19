package az.ingrees.demo.service;

import az.ingrees.demo.controller.Student;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class StudentServiceImp implements StudentService{
    @Override
    public Student get(Long id){
        log.info("Student service get method is working");
        return null;
    }
    @Override
    public Student create(Student employee){
        log.info("Student service create method is working");
        return null;
    }
    @Override
    public Student update(Student employee){
        log.info("Student service update method is working");
        return null;
    }
    @Override
    public void delete(Long id){

    }
}

