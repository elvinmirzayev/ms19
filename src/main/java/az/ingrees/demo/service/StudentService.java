package az.ingrees.demo.service;

import az.ingrees.demo.controller.Student;

public interface StudentService {
    Student get(Long id);

    Student create(Student student);

    Student update(Student student);

    void delete(Long id);
}
